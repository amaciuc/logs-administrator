## logs-administrator ##
Acest proiect are ca scop demonstrarea partii practice a lucrarii de disertatie prezentata in **Iulie 2020**.

Partea practica consta din urmatoarele etape:

### 1. Pornirea generatorului de log-uri: ###
```shell
$ bash ./logs_generator.sh 30 &
```
unde **30** reprezinta intervalul de creare a mesajelor jurnal in secunde.
Se va crea automat un fisier de iesire sub numele **logs_generator.log**.

### 2. Filebeat ###

**a. Descarcarea aplicatiei**
```shell
(
    echo "Downloading the new app ..." &&
    url="https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.8.0-linux-x86_64.tar.gz" &&
    wget -q "${url}" &&
    echo "Unzipping ..." &&
    tar -xf filebeat-7.8.0-linux-x86_64.tar.gz
)
```
Aceasta secventa se va copia (tot cu paranteze) si se va *lipi* in linia de comanda unde se doreste descarcarea aplicatiei.

**b. Pornirea unei instante incarcand fisierul de configurare din acest proiect**
```shell
cd filebeat-7.8.0-linux-x86_64 && 
./filebeat -c ../filebeat.yml
```

### 3. Logstash ###

**a.  Descarcarea aplicatiei logstash si instalarea unui plug-in utilizat pentru comunicarea cu server-ul de baza de date *mariaDB***
```shell
(
    new_app="logstash-7.8.0" &&
    echo "Downloading the new app ..." &&
    url="https://artifacts.elastic.co/downloads/logstash/${new_app}.zip" &&
    wget -q "${url}" &&
    echo "Unzipping ..." &&
    unzip -oq ${new_app}.zip &&
    echo "== Prepare to install the mariaDB plugin used for output ==" &&
    echo -e "Downloading jdbc ..." &&
    jdbc_url="https://downloads.mariadb.com/Connectors/java/connector-java-2.6.0/mariadb-java-client-2.6.0.jar" &&
    wget -q "${jdbc_url}" &&
    mkdir -p ${new_app}/vendor/jar/jdbc/ &&
    mv mariadb-java-client-2.6.0.jar ${new_app}/vendor/jar/jdbc/ &&
    echo "Installing the plugin ..." &&
    # JAVA_HOME=/jdk1.8.0_91 &&
    # PATH=\$JAVA_HOME/bin:$PATH &&
    ${new_app}/bin/logstash-plugin install logstash-output-jdbc
)
```

**b. Pornirea unei instante incarcand fisierul de configurare din acest proiect**

Inainte de pornirea acestea, este necesara importarea [schemei](https://gitlab.com/amaciuc/logs-administrator/-/blob/master/bucket_logs_schema.sql) bazei de date in care *logstash* va scrie mesajele jurnal.

**!OBS** A nu se uita sa se modifice credentialele de logare la baza de date din fisierul de [configurare](https://gitlab.com/amaciuc/logs-administrator/-/blob/master/logstash.conf) a aplicatiei *logstash*. In mod implicit acestea sunt user: root, pass: root.
```shell
(
    new_app="logstash-7.8.0" && 
    cd ${new_app} && 
    ./bin/logstash --path.config ../logstash.conf
)
```

### 4. Elasticsearch / Prometheus / Grafana ###
Instalarea acestora, dar si pornirea unei instante catre fiecare aplicatie se va realiza de catre aplicatia **docker-compose**, care necesita a fi instalata pe masina de test. Fisierul de configurare *.yml* necesar acestei aplicatii este [docker-compose.yml](https://gitlab.com/amaciuc/logs-administrator/-/blob/master/docker-compose.yml)
```
docker-compose up --detach elasticsearch grafana prometheus
```
Prin intermediul fisierului de configurare se vor descarca imaginile de docker necesare, se vor instala si se vor crea servicii (containere) in *background* la imaginile respective. Dupa pornirea acestor servicii, fiecare aplicatie deschide cate un *socket* pentru comunicatie.

**a. Elasticsearch**
  - Adresa URL: http://127.0.0.1:9200/
  - Vizualizarea indecsilor: http://127.0.0.1:9200/_cat/indices?v&pretty

**b. Prometheus** 
  - Adresa URL: http://localhost:9090/
  - Vizualizarea job-urilor de colectare de metrici: http://localhost:9090/targets

**c. Grafana:** http://localhost:3001/
  - user: admin
  - pass: admin

S-a creat un *dashboard* de Grafana si se poate gasi in proiect exportat sub forma de JSON, fisierul este [grafana-dashboard.json](https://gitlab.com/amaciuc/logs-administrator/-/blob/master/grafana-dashboard.json). Practic, la aceasta sectiune: http://localhost:3001/dashboard/import se va aplica codul JSON.

**!OBS** Dupa crearea unei instante de Grafana, este necesara configurarea surselor de date la aceasta sectiune: http://localhost:3001/datasources.
 - Pentru Elasticsearch se va folosi URL-ul de mai sus, la **Index name** se va completa cu `logs-*`, iar la **Version** cu `7.0+`;
 - Pentru Grafana se va seta doar sursa cu URL de mai sus.

### 5. Metricile prometheus ###
Pentru a monitoriza instantele de filebeat, logstash sau oricare alta aplicatie din punct de vedere *hardware*, s-a pregatit o imagine de docker de inglobeaza un script Python, iar acesta, pe baza PID-ului aplicatiei, va exporta informatiile urmatoare: utilizarea CPU-ului, memoriei RAM, spatiul de stocare ocupat de directorul sursa al aplicatiei (pe baza caii fizice), timpul de cand ruleaza aplicatia si daca aceasta ruleaza sau nu.

Pentru a porni un serviciu de acest gen este necesara exportarea a doua variabile de *environment* care sa contina calea fizica catre directorul aplicatiei de monitorizat si PID-ul aplicatiei. Mai jos se va exemplifica pornirea unor servicii.

In acest proiect se poate gasi o [imagine](https://gitlab.com/amaciuc/logs-administrator/-/blob/master/build-docker-image/hardware-monitor-1.0.0.tar.gz) deja exportata, dar sunt pregatite fisiere in directorul `build-docker-image` astfel incat sa se poata crea propria imagine de docker prin intermediul lui **docker-compose**. Fisierul de configurare are definit un astfel de serviciu.
```shell
docker-compose build
```

**!!!** Daca se doreste utilizarea imaginii de docker deja creata, se va rula urmatoarea secventa pentru import:
```shell
(
  image_name="hardware-monitor" &&
  image_tag="1.0.0" &&
  archive_name="${image_name}-${image_tag}" &&

  echo "Downloading compressed archive ..." &&
  wget https://gitlab.com/amaciuc/logs-administrator/-/raw/master/build-docker-image/hardware-monitor-1.0.0.tar.gz &&

  echo "Uncompressing archive ..." &&
  tar xf ${archive_name}.tar.gz &&

  echo "Removing compressed archive ..." &&
  rm -f ${archive_name}.tar.gz &&

  echo "Importing .tar archive into docker ..." &&
  docker load -i ${archive_name}.tar &&

  echo "Removing archive file ..." &&
  rm -f ${archive_name}.tar
)
```

Exemplu de creare a serviciilor de monitorizare a aplicatiilor logstash, filebeat si logs_generator.sh:
```shell
# filebeat
cd filebeat-7.8.0-linux-x86_64 &&
export APP_CWD=$(pwd -P) &&
export APP_PID=$(PIDS="$(ps aux | grep -vE "grep" | grep -E \
"filebeat" | awk '{ print $2 ; }' )"
for PID in $PIDS ; do
  P="$(ls -l /proc/${PID}/cwd 2> /dev/null | awk '{ print $11 ; }')"
  if [[ $P == $(pwd -P) ]]; then
    echo "${PID}"
  fi
done
) &&
echo $APP_PID &&
cd ../ &&
docker-compose up --detach filebeat_prom &&
# logstash
cd logstash-7.8.0 &&
export APP_CWD=$(pwd -P) &&
export APP_PID=$(PIDS="$(ps aux | grep -vE "grep" | grep -E \
"logstash" | awk '{ print $2 ; }' )"
for PID in $PIDS ; do
  P="$(ls -l /proc/${PID}/cwd 2> /dev/null | awk '{ print $11 ; }')"
  if [[ $P == $(pwd -P) ]]; then
    echo "${PID}"
  fi
done
) &&
echo $APP_PID &&
cd ../ &&
docker-compose up --detach logstash_prom &&
# logs_generator.sh
export APP_CWD=$(pwd -P) &&
export APP_PID=$(PIDS="$(ps aux | grep -vE "grep" | grep -E \
"logs_generator.sh" | awk '{ print $2 ; }' )"
for PID in $PIDS ; do
  P="$(ls -l /proc/${PID}/cwd 2> /dev/null | awk '{ print $11 ; }')"
  if [[ $P == $(pwd -P) ]]; then
    echo "${PID}"
  fi
done
) &&
echo $APP_PID &&
docker-compose up --detach logs-generator_prom
```
Graficele in Grafana ar trebui sa arate sub aceasta forma dupa pornirea serviciilor:
![image.png](./image.png)


