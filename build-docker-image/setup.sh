
yum install -y wget gcc-c++ make tar epel-release cmake3 &&
# ========================= Get number of cores ============================== #
# NProc returns 1 sometimes. Experienced since docker 18.09.6
#no_cores=$(nproc) &&
no_cores="$(lscpu | grep "^CPU(s):" | awk '{print $2}')" &&
# ===================== Install Python3.7 (with pip) ========================= #
(
  # - zlib-devel, libffi-devel - Needed for building python3.7
  # - sqlite-devel - Needed for building sqlite module for conan
  # - bzip2-devel bzip2-libs - Needed for building bz2 module of python
  yum install -y zlib-devel libffi-devel sqlite-devel bzip2-devel &&
  # openssl-devel - Needed for building python3.7
  yum install -y openssl-devel &&
  PYTHON_VERSION="3.7.7" &&
  PYTHON_DIR="Python-${PYTHON_VERSION}" &&
  PYTHON_BIN_NAME="python3.7" &&
  PIP_BIN_NAME="pip3.7" &&
  cd ~ &&
  wget "https://www.python.org/ftp/python/$PYTHON_VERSION/${PYTHON_DIR}.tgz" &&
  tar xzf $PYTHON_DIR.tgz &&
  cd $PYTHON_DIR &&
  #pip3.7 is installed also
  PYTHON_INSTALL_PATH="/opt/python/3.7" &&
  mkdir -p ${PYTHON_INSTALL_PATH} &&
  mkdir -p ${PYTHON_INSTALL_PATH}/lib &&
  ./configure --prefix=${PYTHON_INSTALL_PATH} --enable-shared \
              --with-ensurepip=install --enable-optimizations \
              --enable-loadable-sqlite-extensions \
              LDFLAGS="-Wl,-rpath ${PYTHON_INSTALL_PATH}/lib" &&
  make -j$no_cores &&
  make install &&
  cd .. &&
  rm -rf ${PYTHON_DIR}* &&
  (
    cd ${PYTHON_INSTALL_PATH}/bin &&
    ln -sf ${PYTHON_BIN_NAME} python &&
    ln -sf ${PIP_BIN_NAME} pip
  ) &&

  (
    echo "PYTHON_INSTALL_PATH=\"${PYTHON_INSTALL_PATH}\"" &&
    echo "export PATH=\"\${PYTHON_INSTALL_PATH}/bin:\$PATH\"" &&
    echo -n "export LD_LIBRARY_PATH=\"\${PYTHON_INSTALL_PATH}/lib:" &&
    echo "\${PYTHON_INSTALL_PATH}/lib64:\$LD_LIBRARY_PATH\""
  ) >> ~/.bashrc &&

  # this cannot be done because no package fakesystemd is available
  # yum -y swap -- remove systemd systemd-libs -- install fakesystemd &&
  yum remove -y openssl-devel zlib-devel sqlite-devel bzip2-devel &&
  # there's a problem when you simply try to remove libffi-devel
  yum remove -y --setopt=tsflags=noscripts libffi-devel

) &&
. ~/.bashrc &&
# ======================= END - Install Python3.7 ============================ #
# Install dependencies used by collector.py script =========================== #
pip3 install psutil prometheus_client &&
# END - Install dependencies
echo "Finalizing installation ..." &&
# --- Cleanup container
yum clean all &&
exit 0
