# ============================================================================ #
import sys # for handling system exit
import os # for getting current working directory
import time # For sleeping
import datetime # For logging timestamp & parsing uptime from status
import subprocess # Make a shell command
import logging  # Logging
# sys.path.append( os.getcwd() ) # dependencies in main project folder
# Dependencies that have to be pre-solved
from prometheus_client import start_http_server, Info, Gauge
import psutil # cpu usage, ram, etc from system
# ============================================================================ #
prometheus_server_port = 9095
prometheus_collector_interval = 5 # seconds
# ============================================================================ #
# Gauge metrics status
memory_rss = Gauge("memory_rss",
  "Number of Resident Set Size RAM memory bytes used by the app")
up_time = Gauge("up_time",
  "Number of seconds since the app was started")
percent_cpu = Gauge("percent_cpu", "Get the CPU usage of the app as a percentage")
disk_usage = Gauge("disk_usage", "Usage of physical memory")
is_up = Gauge("is_up", "1 If app is running and 0 if it's not. "
              "Based on existence of PID")
# ============================================================================ #
def collect_process_metrics():
  try:
    # Validate pid
    try:
      pid = int(host_pid)
    except ValueError:
      logger.error(f"Invalid PID: \"{host_pid}\"")
      is_up.set(0)
      return
    try:
      process = psutil.Process(pid=pid)
    except psutil.NoSuchProcess as err:
      logger.error(f"Process not found (\"{err}\")")
      is_up.set(0)
      return
    memory_rss.set(process.memory_info().rss)
    my_date = datetime.datetime.fromtimestamp(process.create_time())
    my_date = datetime.datetime.now() - my_date
    up_time.set(my_date.total_seconds())
    percent_cpu.set(process.cpu_percent(interval = 1))
    cmd = "du -bcs " + app_path + " | cut -f 1 | head -1"
    exec_cmd = subprocess.run(cmd, shell = True, stdout = subprocess.PIPE,
                              universal_newlines = True)
    space_usage = exec_cmd.stdout.split()[0]
    disk_usage.set(space_usage)
    is_up.set(1)
  except Exception as err:
    logger.error(f"Unexpected error while creating metrics: \"{err}\"")
# ============================================================================ #
if __name__ == '__main__':
  # =====- Setting up logger -=====

  # Levels: CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET
  logger = logging.getLogger()

  # Logging messages which are less severe than `level` will be ignored
  logger.setLevel(logging.INFO)

  # Log to file
  # logger_handler = logging.FileHandler(os.path.splitext(__file__)[0]+".log

  # Log to stdout
  logger_handler=logging.StreamHandler(sys.stdout)

  logger_handler.setFormatter(logging.Formatter(\
      "[%(asctime)s]:%(levelname)s:  %(message)s  :%(levelname)s", "%d-%m-%Y %T"))
  logger.addHandler(logger_handler)
  # ===============
  # Some messages as beginning
  logger.info("")
  logger.info(f"{__file__} was started.")
  logger.info(f"Logging level set to: \"{logging.getLevelName(logger.level)}\".")

  # Prepare proc from host
  if not os.path.isdir('/host/proc'):
    logger.error("\"/host/proc\" doesn't exist")
  psutil.PROCFS_PATH='/host/proc'

  # Get PID to be monitored
  if 'HOST_PID' in os.environ:
    host_pid = os.environ.get('HOST_PID')
    logger.info(f"Process to be monitored: \"{host_pid}\"")
  else:
    logger.error("The \"HOST_PID\" env variable is not set")

  # Get path to the monitored app
  if 'APP_CWD' in os.environ:
    app_path = os.environ.get('APP_CWD')
    logger.info(f"Path to the monitored app: \"{app_path}\"")
  else:
    logger.error("The \"APP_CWD\" env variable is not set")

  # Get prometheus port
  if 'PROMETHEUS_PORT' in os.environ:
    try:
      prometheus_server_port = int(os.environ.get('PROMETHEUS_PORT'))
    except ValueError:
      logger.error(f"Invalid PROMETHEUS_PORT: \"{PROMETHEUS_PORT}\"")
      sys.exit(1)
  else:
    logger.info("The \"PROMETHEUS_PORT\" is not set. "
                "Prometheus will use the default value for port")
  logger.info(f"Prometheus will start with \"{prometheus_server_port}\" port")

  # Get prometheus interval in seconds
  if 'PROMETHEUS_INTERVAL' in os.environ:
    try:
      prometheus_collector_interval = int(os.environ.get("PROMETHEUS_INTERVAL"))
    except ValueError:
      logger.error(f"Invalid PROMETHEUS_INTERVAL: \"{PROMETHEUS_INTERVAL}\"")
      sys.exit(1)
  else:
    logger.info("The \"PROMETHEUS_INTERVAL\" is not set. "
                "The collector will use the default interval")
  logger.info("The metrics are created at every "
              f"\"{prometheus_collector_interval}\" seconds")

  # First collect metrics, before exposing them, to avoid exposing null values
  collect_process_metrics()

  # Start up the server to expose the metrics.
  try:
    logger.info("Starting prometheus server ...")
    start_http_server(prometheus_server_port)
  except Exception as err:
    logger.error(f"Exception raised while starting prometheus server: \"{err}\"")

  logger.info("Data collection is started")
  # Start collecting data:
  while True:
    time.sleep(prometheus_collector_interval)
    collect_process_metrics()
# ============================================================================ #
