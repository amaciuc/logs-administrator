-- MySQL dump 10.13  Distrib 5.6.44, for Linux (x86_64)
--
-- Host: 192.168.220.2    Database: qppr_logs
-- ------------------------------------------------------
-- Server version	5.6.43

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `host`
--

DROP TABLE IF EXISTS `host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `host` (
  `ho_host_id` smallint(3) NOT NULL AUTO_INCREMENT,
  `ho_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `ho_short_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `ho_ip` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ho_host_id`),
  UNIQUE KEY `ho_name_idx` (`ho_name`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ignore_message`
--

DROP TABLE IF EXISTS `ignore_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ignore_message` (
  `igm_ignore_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `igm_message` blob,
  `igm_last_message_appearance` datetime DEFAULT NULL,
  `igm_last_notification` datetime DEFAULT NULL,
  `igm_start_ignore` datetime DEFAULT NULL,
  `igm_stop_ignore` datetime DEFAULT '2020-01-01 00:00:00',
  `igm_is_enabled` tinyint(1) DEFAULT NULL COMMENT 'yes / no',
  `igm_auto_inserted` tinyint(1) DEFAULT '0' COMMENT 'yes / no',
  PRIMARY KEY (`igm_ignore_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_file`
--

DROP TABLE IF EXISTS `log_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_file` (
  `lf_file_id` int(20) NOT NULL AUTO_INCREMENT,
  `lf_mi_monitor_instance_id` smallint(6) NOT NULL,
  `lf_file_name` varchar(100) NOT NULL,
  `lf_timestamp` datetime NOT NULL,
  `lf_is_archived` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`lf_file_id`),
  UNIQUE KEY `UNIQUE_idx` (`lf_mi_monitor_instance_id`,`lf_file_name`),
  KEY `lf_mi_monitor_instance_id_idx` (`lf_mi_monitor_instance_id`),
  KEY `lf_file_name` (`lf_file_name`),
  CONSTRAINT `FK_lf_mi_monitor_instance_id` FOREIGN KEY (`lf_mi_monitor_instance_id`) REFERENCES `monitor_instance` (`mi_monitor_instance_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_message`
--

DROP TABLE IF EXISTS `log_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_message` (
  `lm_log_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `lm_fl_file_id` int(11) NOT NULL,
  `lm_message` blob NOT NULL,
  `lm_orig_message_timestamp` varchar(50) NOT NULL,
  `lm_timestamp` datetime NOT NULL,
  `lm_vl_verbosity_level_id` smallint(6) NOT NULL,
  PRIMARY KEY (`lm_log_message_id`),
  KEY `FK_lm_fl_file_id_idx` (`lm_fl_file_id`),
  KEY `FK_lm_vl_verbosity_level_id_idx` (`lm_vl_verbosity_level_id`),
  CONSTRAINT `FK_lm_fl_file_id` FOREIGN KEY (`lm_fl_file_id`) REFERENCES `log_file` (`lf_file_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_lm_vl_verbosity_level_id` FOREIGN KEY (`lm_vl_verbosity_level_id`) REFERENCES `verbosity_level` (`vl_verbosity_level_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `monitor_instance`
--

DROP TABLE IF EXISTS `monitor_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monitor_instance` (
  `mi_monitor_instance_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `mi_ho_host_id` smallint(6) NOT NULL,
  `mi_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `mi_path` varchar(100) CHARACTER SET utf8 NOT NULL,
  `mi_timestamp` datetime NOT NULL,
  PRIMARY KEY (`mi_monitor_instance_id`),
  UNIQUE KEY `UNIQUE_idx` (`mi_name`,`mi_path`, `mi_ho_host_id`),
  KEY `mi_name_idx` (`mi_name`),
  KEY `mi_path_idx` (`mi_path`),
  KEY `FK_mi_ho_host_id_idx` (`mi_ho_host_id`),
  CONSTRAINT `FK_mi_ho_host_id` FOREIGN KEY (`mi_ho_host_id`) REFERENCES `host` (`ho_host_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `verbosity_level`
--

DROP TABLE IF EXISTS `verbosity_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verbosity_level` (
  `vl_verbosity_level_id` smallint(2) NOT NULL AUTO_INCREMENT,
  `vl_value` smallint(2) DEFAULT NULL,
  `vl_type` char(10) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`vl_verbosity_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

--
-- Dumping data for table `verbosity_level`
--

LOCK TABLES `verbosity_level` WRITE;
/*!40000 ALTER TABLE `verbosity_level` DISABLE KEYS */;
INSERT INTO `verbosity_level` VALUES (1,0,'VL_NOOUT'),(2,1,'VL_ALWAYS'),(3,2,'VL_ERROR'),(4,3,'VL_WARNING'),(5,4,'VL_INFO'),(6,5,'VL_DEBUG'),(7,6,'VL_MAX');
/*!40000 ALTER TABLE `verbosity_level` ENABLE KEYS */;
UNLOCK TABLES;

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-03 18:08:23
