# ============================================================================ #
# Printare mesaj jurnal
# $1 trebuie sa fie tipul de mesaj (INFO/ERROR/WARNING/DEBUG)
# $2 trebuie sa fie mesajul propriu zis
# ============================================================================ #
function log {
  log_folder="$(cd `dirname $0` && pwd)" &&
  log_file_name=$(basename -- "$0") &&
  log_file_name="${log_file_name%.*}.log" &&
  echo -e "["$(date +"%d-%m-%Y %T")"]:$1:  $2  :$1" >> "$log_folder/$log_file_name"
}

log "INFO" "Script-ul a pornit"

if [[ "$#" != 1 ]]; then
  log "ERROR" "Programul trebuie pornit cu numarul de seconde (Ex: logs_generator.sh 10)"
  exit 1
fi
my_sleep=$1

inc=0

while true; do
  ((inc++))
  log "INFO" "Mesajul \"$inc\" este informatic"
  sleep 1
  ((inc++))
  log "WARNING" "Mesajul \"$inc\" este de avertizare"
  sleep 1
  ((inc++))
  log "DEBUG" "Mesajul \"$inc\" este de depanare"
  sleep 1
  ((inc++))
  log "ERROR" "Mesajul \"$inc\" este de eroare"
  log "INFO" "Se asteapta [$my_sleep] secunde pana la urmatoarea iteratie"
  sleep $my_sleep
done
